from django.contrib.auth.forms import UserCreationForm  , AuthenticationForm, UsernameField
from django.contrib.auth.models import User
from django import forms


class RegistrationForms(UserCreationForm):

    password1 = forms.CharField(label ='',strip=False,
                               widget = forms.PasswordInput(attrs={'placeholder':'Contraseña','class':'form-control'}),
                               help_text='Inserta una contraseña',)
    password2 = forms.CharField(label ='',strip=False,
                               widget= forms.PasswordInput(attrs={'placeholder':'Confirmar Contraseña','class':'form-control'}),
                               help_text="Confirma tu contraseña",)                             
    class Meta:
        
        model = User
        fields = ['username','email','password1','password2',]
        labels = {'username':'',
                  'email':'',
                  'password1':'',
                  'email':'',}

# crear comprobacion

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Email ya exite.')
        return email 



    
  