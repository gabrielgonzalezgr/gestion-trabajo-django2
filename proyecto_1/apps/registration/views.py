from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm , PasswordResetForm , UserCreationForm
from django.contrib.auth.views import LoginView , PasswordChangeView ,PasswordChangeDoneView , PasswordResetView  
from .forms import RegistrationForms 
from django import forms

# importar una vista generica que creeara al usuario
from django.views.generic import CreateView
# para revertir las url 
from django.urls import reverse_lazy

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Create your views here.

class RegistrationView(CreateView):

    form_class = RegistrationForms
    template_name = 'registration/registration.html'
    
    def get_success_url(self):
        return reverse_lazy('registration:login') + '?register'

    def get_form(self,form_class=None):
        # modificar el wiget en tiempo real para no perder las validaciones por defecto 
        form = super(RegistrationView, self).get_form()
        form.fields['username'].widget = forms.TextInput(attrs={'placeholder':'Nombre de Usuario','class':'form-control'})
        form.fields['username'].help_text = 'Introduce un nombre de usuario'
        form.fields['email'].widget = forms.TextInput(attrs={'placeholder':'Correo Electronico','class':'form-control'})
        form.fields['email'].help_text ="Introduce un correo electronico."
        return form 

class LoginView(LoginView):

    form_class = AuthenticationForm
    template_name = 'registration/login.html'

    def get_form(self,form_class=None):
        # modificar el wiget en tiempo real para no perder las validaciones por defecto 
        form = super(LoginView, self).get_form()
        form.fields['username'].widget = forms.TextInput(attrs={'placeholder':' Nombre de Usuario','class':'form-control'})
        form.fields['password'].widget = forms.TextInput(attrs={'placeholder':' Password','class':'form-control','type':'password'})
        return form


@method_decorator(login_required, name='dispatch')
class ChangePasswordView(PasswordChangeView):

    form_class = PasswordChangeForm
    template_name = 'registration/passwordchange.html'
    success_url = reverse_lazy('done_password')
    

class ResetPasswordView(PasswordResetView):

    form_class = PasswordResetForm
    template_name = 'registration/passwordreset.html'

    
@method_decorator(login_required, name='dispatch')
class DonePasswordView(PasswordChangeDoneView):

    template_name = 'registration/passworddone.html'
    
    