from django.urls import path
from .views import CreateContact, ListViewContact,Contact,UpdateContact,DeleteViewContact

app_name="contact"

urlpatterns = [
    path('',ListViewContact.as_view(),name="contacts"),
    path('create/',CreateContact.as_view(),name='create-contact' ),
    path('update/<int:pk>/',UpdateContact.as_view(), name='update-contact' ),
    path('contact/<int:pk>/',Contact.as_view(),name="detail-contact" ),
    path('delete/<int:pk>/',DeleteViewContact.as_view(),name="delete-contact" ),
   # path('/contacts/delete', ),
    
]