from django.shortcuts import render, redirect
from django.views.generic import CreateView, UpdateView , DeleteView,ListView,DetailView
from django import forms

from django.utils.decorators import method_decorator

from django.contrib.auth.decorators import login_required
#
from django.urls import reverse_lazy
from django import template
# Create your views here.
from .forms import ContactsFoms
from .models import ContactsModels
from apps.issue.models import IssueModel



@method_decorator(login_required, name='dispatch')
class CreateContact(CreateView):
    template_name = 'contact/create_contact.html'
    model = ContactsModels
    form_class = ContactsFoms

    def post(self, request, *args, **kwargs):
        self.object = None
        print(request.POST)
        return super().post(request, *args, **kwargs)


    def get_success_url(self):
        return reverse_lazy('contact:create-contact') + '?client_create'
    
@method_decorator(login_required, name='dispatch')
class UpdateContact(UpdateView):
    template_name = 'contact/update_contact.html'
    model = ContactsModels
    form_class = ContactsFoms


    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        contactid=self.kwargs['pk']
        return super().get(request, *args, **kwargs)


    


    def get_success_url(self):
           #Tengo que capturar el pk de la query y pasarlo a la funcion de return en success #
          contactid=self.kwargs['pk']
          return reverse_lazy('contact:detail-contact', kwargs={'pk': contactid}) + "?client_updated"

@method_decorator(login_required, name='dispatch')
class Contact(DetailView):
    model = ContactsModels
    template_name = 'contact/detail_contact.html'

    def get_context_data(self, *args, **kwargs):
        context = super(Contact, self).get_context_data(*args, **kwargs)
        id=self.kwargs['pk']
        context['issues'] =IssueModel.objects.filter(cliente_id=id)
        context['contacts'] =ContactsModels.objects.all()
        return context


@method_decorator(login_required, name='dispatch')
class DeleteViewContact(DeleteView):
    model = ContactsModels
    template_name = 'contact/delete_contact.html'
    

    def get_context_data(self, *args, **kwargs):
        context = super(DeleteViewContact, self).get_context_data(*args, **kwargs)
        context['contacts'] = ContactsModels.objects.all()
        id=self.kwargs['pk']
        context['issues'] =IssueModel.objects.filter(cliente_id=id)
        context['counts'] =IssueModel.objects.filter(cliente_id=id).count()
        return context

    def get_success_url(self):
        return reverse_lazy('contact:contacts') + '?delete-client'


@method_decorator(login_required, name='dispatch')
class ListViewContact(ListView):
    template_name = 'contact/list_contact.html'
    
    model = ContactsModels