from django import forms
from .models import ContactsModels

class ContactsFoms(forms.ModelForm):
    
    nombre = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control",
                                                           "name":"name",
                                                           "placeholder":"Nombre",
                                                           }))
    direccion = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control",
                                                           "name":"direct",
                                                           "placeholder":"Direccion",
                                                           }))
    telefono = forms.CharField(error_messages={"invalid":"ya Existe ese numero de telefono"},widget=forms.TextInput(attrs={"class":"form-control",
                                                           "name":"phone",
                                                           "placeholder":"Telefono",
                                                           }))
    s_telefono = forms.CharField(error_messages={"invalid":"ya Existe ese numero de telefono"},widget=forms.TextInput(attrs={"class":"form-control",
                                                           "name":"phone2",
                                                           "placeholder":"Segundo Telefono",
                                                           }))
    mail = forms.EmailField(widget=forms.TextInput(attrs={"class":"form-control",
                                                           "name":"mail",
                                                           "type":"email",
                                                           "placeholder":"Mail",
                                                           }))
    comentario = forms.CharField(required=False,widget=forms.Textarea(attrs={"class":"form-control textarea-autosize",
                                                           "name":"name",
                                                           "id":"comentariocliente",
                                                           "placeholder":"Comentario",
                                                           "rows":"3"
                                                           }))

    class Meta:
        model = ContactsModels
        fields = '__all__'



