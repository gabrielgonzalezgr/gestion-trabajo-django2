from django.db import models

# Create your models here.


class ContactsModels(models.Model):

    nombre = models.CharField(max_length=254,unique=True)
    direccion =models.CharField(max_length=254)
    telefono =models.SlugField(max_length=50,unique=True,error_messages={'unique':"ya Existe  este numero de telefono."})
    s_telefono = models.SlugField(max_length=50,unique=True,error_messages={'unique':"ya Existe este numero de telefono."})
    mail = models.EmailField(max_length=255,unique=True,error_messages={'unique':"ya Existe ese Correo electronico."})
    comentario =models.TextField(blank=True,max_length=254 ,null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)    

    def __str__(self):
        return str(self.nombre)

        #crear table de direccioon


        