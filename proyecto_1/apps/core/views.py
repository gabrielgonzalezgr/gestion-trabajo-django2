from django.shortcuts import render
from django.views.generic import TemplateView ,ListView
from apps.issue.models import IssueModel
import datetime
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
# Create your views here.


@method_decorator(login_required, name='dispatch')
class HomeViews(ListView):
    template_name = 'core/home.html'
    model= IssueModel
    context_object_name = 'issue'
    
    def get_queryset(self):

        today_date = datetime.datetime.now().date()
        day=15
        substration_day = datetime.timedelta(days=day)
        before = today_date -  substration_day

        queryset = {'issue_all': IssueModel.objects.all(), 
                    'issue_reminder': IssueModel.objects.all().filter(fecha__lte=before)}

        return queryset

    

    