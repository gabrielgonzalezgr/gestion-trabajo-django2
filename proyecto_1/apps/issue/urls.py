
from django.urls import path 
from .views import IssueCreateView, CategoriesView , typesView,IssueDetailView, IssueListView ,IssueUpdateView, IssueDeleteView

app_name="issue"

urlpatterns = [
    path('create/', IssueCreateView.as_view(), name='create_issue'),
    path('detail/<int:pk>/', IssueDetailView.as_view(), name='detail_issue'),
    path('list_issue/', IssueListView.as_view(), name='list_issue'),
    path('update/<int:pk>/', IssueUpdateView.as_view(), name='update_issue'),
    path('delete/<int:pk>/', IssueDeleteView.as_view(), name='delete_issue'),
    #path( path('detail/<int:pk>/', IssueDetailView.as_view(), name='detail_issue'),


    path('categories/', CategoriesView.as_view(), name='create_categories'),
    path('types/', typesView.as_view(), name='create-types'),


    ]