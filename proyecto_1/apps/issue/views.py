from django.shortcuts import render
from .forms import IssueForms
from django.views.generic import CreateView , UpdateView , DetailView , ListView ,DeleteView
from .models import IssueModel , CategoriesModels , TypesModels ,STATUS
from django.urls import reverse_lazy

from .forms import IssueForms ,CategoryForms, TypeForms
from django import forms
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required




# Create your views here.
@method_decorator(login_required, name='dispatch')
class IssueCreateView(CreateView):
    template_name = 'issue/create_issue.html'
    model  = IssueModel
    form_class = IssueForms

    def post(self, request, *args, **kwargs):
        self.object = None   
        return super().post(request, *args, **kwargs)


    def get_success_url(self):
        return reverse_lazy('core:home-core') + '?created_issue'

    def get_form(self,form_class=None):
        # modificar el wiget en tiempo real para no perder las validaciones por defecto 
        form = super(IssueCreateView, self).get_form()
        form.fields['estado'].widget = forms.Select(choices=STATUS,attrs={'placeholder':'Nombre de Usuario','class':'form-control'})
        return form



@method_decorator(login_required, name='dispatch')
class IssueDetailView(DetailView):
    template_name = 'issue/detail_issue.html'
    model  = IssueModel
    context_object_name="object"

@method_decorator(login_required, name='dispatch')    
class IssueListView(ListView):
    template_name = 'issue/list_issue.html'
    model  = IssueModel

@method_decorator(login_required, name='dispatch')
class IssueUpdateView(UpdateView):
    template_name = 'issue/update_issue.html'
    model  = IssueModel
    form_class = IssueForms

    def get_success_url(self):
        issueid=self.kwargs['pk']
        return reverse_lazy('issue:detail_issue',kwargs={'pk': issueid}) + '?update_issue'


@method_decorator(login_required, name='dispatch')
class CategoriesView(CreateView):
    template_name = 'issue/create_categories.html'
    form_class = CategoryForms
    model = CategoriesModels
    success_url = reverse_lazy('issue:create-types')

@method_decorator(login_required, name='dispatch')
class typesView(CreateView):

    template_name = 'issue/create_types.html'
    form_class = TypeForms
    model = TypesModels
    success_url = reverse_lazy('issue:create_issue')


@method_decorator(login_required, name='dispatch')
class IssueDeleteView(DeleteView):
    model = IssueModel
    template_name = 'issue/delete_issue.html'




    def get_success_url(self):
        return reverse_lazy('core:home-core') + '?delete_issue'