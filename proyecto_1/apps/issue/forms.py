from django import forms
from .models import CategoriesModels ,IssueModel ,TypesModels, STATUS
from apps.contact.models import ContactsModels


class IssueForms(forms.ModelForm):

    cliente = forms.ModelChoiceField(queryset=ContactsModels.objects.all(),empty_label=None,widget=forms.Select(attrs={"class":"form-control","name":"cliente",}))
    
    ubicacion = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","name":"ubicacion","placeholder":"Ubicación",}))  

    fecha = forms.DateField(input_formats=['%Y-%m-%d'], widget=forms.DateInput(format=('%Y-%m-%d'),attrs={"class":"form-control","type":"date" }))

    hora = forms.TimeField(input_formats=['%H:%M'],widget=forms.TimeInput(format=('%H:%M'),attrs={"class":"form-control","type":"time", }))                                                                                               
    
    categoria = forms.ModelChoiceField(queryset=CategoriesModels.objects.all(),empty_label=None,widget=forms.Select(attrs={"class":"form-control","name":"categoria",}))                                                               
    
    tipo = forms.ModelChoiceField(queryset=TypesModels.objects.all(),empty_label=None,widget=forms.Select(attrs={"class":"form-control","name":"tipo",}))
    
    instrucciones = forms.CharField(required=False,widget=forms.Textarea(attrs={"class":"form-control","name":"instrucciones", "id":"instrucciones","placeholder":"Instrucciones","rows":"3",}))  



    class Meta:
        model = IssueModel
        fields = ['cliente','ubicacion','fecha','hora','categoria','tipo','instrucciones','estado']

       

class CategoryForms(forms.ModelForm):

    nombre = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","name":"ubicacion","placeholder":"Nombre de Categoria",})) 

    class Meta:
        model = CategoriesModels
        fields = '__all__'


class TypeForms(forms.ModelForm):

    nombre = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","name":"ubicacion","placeholder":"Nombre de tipo",})) 
    categoria = forms.ModelChoiceField(queryset=CategoriesModels.objects.all(),empty_label=None,widget=forms.Select(attrs={"class":"form-control","name":"Tipo",}))

    class Meta:
        model = TypesModels
        fields = '__all__'

