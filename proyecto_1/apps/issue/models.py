from django.db import models
from apps.contact.models import ContactsModels
from datetime import datetime
from django import forms


STATUS = (('Abierto','Abierto'),
            ('Cerrado','Cerrado'),)
# Create your models here.

class CategoriesModels(models.Model):
    nombre = models.CharField(max_length=255 , unique=True )
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)


    def __str__(self):
        return self.nombre


class TypesModels(models.Model):
    nombre = models.CharField(max_length=255 , unique=True )
    categoria = models.ForeignKey(CategoriesModels,on_delete=models.CASCADE, null=False , blank=False)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.nombre

class IssueModel(models.Model):

    cliente = models.ForeignKey(ContactsModels , on_delete=models.CASCADE,
                                                    null=False, blank=False)
    ubicacion = models.CharField(max_length=255)
    fecha = models.DateField()
    hora =models.TimeField()
    categoria = models.ForeignKey(CategoriesModels,on_delete=models.CASCADE,
                                                 null=False, blank=False)
    tipo = models.ForeignKey(TypesModels,on_delete=models.CASCADE,
                                        null=False, blank=False)
    instrucciones = models.TextField(blank=True,null=True)

    STATUS = (('Abierto','Abierto'),
                ('Cerrado','Cerrado'),

    )
    MOD_WORK = (('F','Full'),
              ('M','Mitad'),
    
    )

    estado = models.CharField(max_length=13, choices=STATUS ,default='Abierto')

    #fecha_cierre = models.DateField()
    
    mod_work = models.CharField(max_length=10, choices=MOD_WORK ,default='Full')
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    def clean(self):
    
        if not (self.fecha >= datetime.now().date()):
            
            raise forms.ValidationError({"fecha":"Este campo no puede ser menor a hoy"})
        
    class Meta:
        ordering=['fecha','hora']

    def __str__(self):
        return str(self.cliente)